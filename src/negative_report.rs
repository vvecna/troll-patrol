use crate::{
    bridge_verification_info::BridgeVerificationInfo, crypto::EciesCiphertext, get_date,
    BridgeDistributor, COUNTRY_CODES, MAX_BACKDATE,
};

use curve25519_dalek::scalar::Scalar;
use lox_library::{bridge_table::BridgeLine, cred::Lox};
use rand::RngCore;
use serde::{Deserialize, Serialize};
use sha1::{Digest, Sha1};
use sha3::Sha3_256;
use x25519_dalek::{PublicKey, StaticSecret};

#[derive(Debug, Serialize)]
pub enum NegativeReportError {
    DateInFuture,
    DateInPast,          // report is more than MAX_BACKDATE days old
    FailedToDecrypt,     // couldn't decrypt to SerializableNegativeReport
    FailedToDeserialize, // couldn't deserialize to NegativeReport
    InvalidCountryCode,
    MissingCountryCode,
}

/// A report that the user was unable to connect to the bridge
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct NegativeReport {
    /// hashed fingerprint (SHA-1 hash of 20-byte bridge ID)
    pub fingerprint: [u8; 20],

    /// user's country code
    pub country: String,

    /// today's Julian date
    pub date: u32,

    /// the bridge distributor, e.g., Lox, Https, or Moat
    pub distributor: BridgeDistributor,

    /// some way to prove knowledge of bridge
    bridge_pok: ProofOfBridgeKnowledge,

    /// a random nonce used in the bridge_pok
    pub nonce: [u8; 32],
}

impl NegativeReport {
    pub fn new(
        bridge_id: [u8; 20],
        bridge_pok: ProofOfBridgeKnowledge,
        country: String,
        date: u32,
        nonce: [u8; 32],
        distributor: BridgeDistributor,
    ) -> Self {
        let mut hasher = Sha1::new();
        hasher.update(bridge_id);
        let fingerprint: [u8; 20] = hasher.finalize().into();
        Self {
            fingerprint,
            bridge_pok,
            country,
            date,
            nonce,
            distributor,
        }
    }

    pub fn from_bridgeline(
        bridgeline: BridgeLine,
        country: String,
        distributor: BridgeDistributor,
    ) -> Self {
        let date = get_date();
        let mut rng = rand::thread_rng();
        let mut nonce = [0; 32];
        rng.fill_bytes(&mut nonce);
        let bridge_pok = ProofOfBridgeKnowledge::HashOfBridgeLine(HashOfBridgeLine::new(
            &bridgeline,
            date,
            nonce,
        ));
        Self::new(
            bridgeline.fingerprint,
            bridge_pok,
            country,
            date,
            nonce,
            distributor,
        )
    }

    pub fn from_lox_bucket(bridge_id: [u8; 20], bucket: Scalar, country: String) -> Self {
        let date = get_date();
        let mut rng = rand::thread_rng();
        let mut nonce = [0; 32];
        rng.fill_bytes(&mut nonce);
        let bridge_pok =
            ProofOfBridgeKnowledge::HashOfBucket(HashOfBucket::new(&bucket, date, nonce));
        Self::new(
            bridge_id,
            bridge_pok,
            country,
            date,
            nonce,
            BridgeDistributor::Lox,
        )
    }

    pub fn from_lox_credential(bridge_id: [u8; 20], cred: &Lox, country: String) -> Self {
        NegativeReport::from_lox_bucket(bridge_id, cred.bucket, country)
    }

    pub fn encrypt(self, server_pub: &PublicKey) -> EncryptedNegativeReport {
        EncryptedNegativeReport {
            date: self.date,
            ciphertext: EciesCiphertext::encrypt(
                &bincode::serialize(&self.to_serializable_report()).unwrap(),
                server_pub,
            )
            .unwrap(),
        }
    }

    /// Convert report to a serializable version
    pub fn to_serializable_report(self) -> SerializableNegativeReport {
        SerializableNegativeReport {
            fingerprint: self.fingerprint,
            bridge_pok: self.bridge_pok,
            country: self.country,
            date: self.date,
            nonce: self.nonce,
            distributor: self.distributor,
        }
    }

    /// Serializes the report, eliding the underlying process
    pub fn to_json(self) -> String {
        serde_json::to_string(&self.to_serializable_report()).unwrap()
    }

    /// Deserializes the report, eliding the underlying process
    pub fn from_json(str: String) -> Result<Self, NegativeReportError> {
        match serde_json::from_str::<SerializableNegativeReport>(&str) {
            Ok(v) => v.to_report(),
            Err(_) => Err(NegativeReportError::FailedToDeserialize),
        }
    }

    /// Deserializes the report from slice, eliding the underlying process
    pub fn from_slice(slice: &[u8]) -> Result<Self, NegativeReportError> {
        match serde_json::from_slice::<SerializableNegativeReport>(slice) {
            Ok(v) => v.to_report(),
            Err(_) => Err(NegativeReportError::FailedToDeserialize),
        }
    }

    /// Verify the report
    pub fn verify(self, bridge_info: &BridgeVerificationInfo) -> bool {
        match self.bridge_pok {
            ProofOfBridgeKnowledge::HashOfBridgeLine(pok) => {
                let hash = HashOfBridgeLine::new(&bridge_info.bridge_line, self.date, self.nonce);
                hash == pok
            }
            ProofOfBridgeKnowledge::HashOfBucket(pok) => {
                for b in &bridge_info.buckets {
                    let hash = HashOfBucket::new(b, self.date, self.nonce);
                    if hash == pok {
                        return true;
                    }
                }
                false
            }
        }
    }
}

/// (De)serializable negative report object which must be consumed by the
/// checking function before it can be used
#[derive(Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct SerializableNegativeReport {
    pub fingerprint: [u8; 20],
    pub country: String,
    pub date: u32,
    pub distributor: BridgeDistributor,
    bridge_pok: ProofOfBridgeKnowledge,
    pub nonce: [u8; 32],
}

impl SerializableNegativeReport {
    pub fn to_report(self) -> Result<NegativeReport, NegativeReportError> {
        if self.country.is_empty() {
            return Err(NegativeReportError::MissingCountryCode);
        }
        if !COUNTRY_CODES.contains(self.country.as_str()) {
            return Err(NegativeReportError::InvalidCountryCode);
        }
        let date = get_date();
        if self.date > date {
            return Err(NegativeReportError::DateInFuture);
        }
        if self.date < date - MAX_BACKDATE {
            return Err(NegativeReportError::DateInPast);
        }
        Ok(NegativeReport {
            fingerprint: self.fingerprint,
            bridge_pok: self.bridge_pok,
            country: self.country.to_string(),
            date: self.date,
            nonce: self.nonce,
            distributor: self.distributor,
        })
    }
}

/// Negative reports should be sent encrypted. This struct provides an
/// encrypted serializable negative report.
#[derive(Serialize, Deserialize)]
pub struct EncryptedNegativeReport {
    /// The date field in the report. This is used to determine which key to use
    /// to decrypt the report.
    pub date: u32,
    ciphertext: EciesCiphertext,
}

impl EncryptedNegativeReport {
    pub fn decrypt(self, secret: &StaticSecret) -> Result<NegativeReport, NegativeReportError> {
        match self.ciphertext.decrypt(secret) {
            Ok(m) => match bincode::deserialize::<SerializableNegativeReport>(&m) {
                Ok(ser_report) => ser_report.to_report(),
                Err(_) => Err(NegativeReportError::FailedToDeserialize),
            },
            Err(_) => Err(NegativeReportError::FailedToDecrypt),
        }
    }
}

/// Proof that the user knows (and should be able to access) a given bridge
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub enum ProofOfBridgeKnowledge {
    /// Hash of bridge line as proof of knowledge of bridge line
    HashOfBridgeLine(HashOfBridgeLine),

    /// Hash of bucket ID for Lox user
    HashOfBucket(HashOfBucket),
}

/// Hash of bridge line to prove knowledge of that bridge
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct HashOfBridgeLine {
    hash: [u8; 32],
}

impl HashOfBridgeLine {
    pub fn new(bl: &BridgeLine, date: u32, nonce: [u8; 32]) -> Self {
        let mut hasher = Sha3_256::new();
        hasher.update(date.to_le_bytes());
        hasher.update(nonce);
        hasher.update(bincode::serialize(&bl).unwrap());
        let hash: [u8; 32] = hasher.finalize().into();
        Self { hash }
    }
}

/// Hash of bucket ID to prove knowledge of bridges in that bucket
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Serialize, Deserialize)]
pub struct HashOfBucket {
    hash: [u8; 32],
}

impl HashOfBucket {
    pub fn new(bucket: &Scalar, date: u32, nonce: [u8; 32]) -> Self {
        let mut hasher = Sha3_256::new();
        hasher.update(date.to_le_bytes());
        hasher.update(nonce);
        hasher.update(bucket.to_bytes());
        let hash: [u8; 32] = hasher.finalize().into();
        Self { hash }
    }
}
