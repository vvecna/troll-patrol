use curve25519_dalek::Scalar;
use ed25519_dalek::VerifyingKey;
use lox_library::bridge_table::BridgeLine;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

/// Information that needs to be known to verify a Troll Patrol report
#[derive(Debug, Serialize, Deserialize)]
pub struct BridgeVerificationInfo {
    /// BridgeLine for this bridge
    pub bridge_line: BridgeLine,

    /// Buckets containing this bridge if this bridge is a Lox bridge
    pub buckets: HashSet<Scalar>,

    /// Key used to verify bridge tokens
    pub pubkey: Option<VerifyingKey>,
}
