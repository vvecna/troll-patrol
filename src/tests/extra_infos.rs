use crate::{simulation::extra_infos_server, *};

use std::{collections::HashSet, time::Duration};
use tokio::{spawn, time::sleep};

#[tokio::test]
async fn test_download_extra_infos() {
    let bridge_to_test =
        array_bytes::hex2array("72E12B89136B45BBC81D1EF0AC7DDDBB91B148DB").unwrap();

    // Open test database
    let db: Db = sled::open("test_db_dei").unwrap();

    // Delete all data in test DB
    db.clear().unwrap();
    assert!(!db.contains_key("bridges").unwrap());
    assert!(!db.contains_key(bridge_to_test).unwrap());

    // Download and process recent extra-infos files
    update_extra_infos(
        &db,
        "https://collector.torproject.org/recent/bridge-descriptors/extra-infos/",
    )
    .await
    .unwrap();

    // Check that DB contains information on a bridge with high uptime
    assert!(db.contains_key("bridges").unwrap());
    let bridges: HashSet<[u8; 20]> =
        bincode::deserialize(&db.get("bridges").unwrap().unwrap()).unwrap();
    assert!(bridges.contains(&bridge_to_test));
    assert!(db.contains_key(bridge_to_test).unwrap());
    let _bridge_info: BridgeInfo =
        bincode::deserialize(&db.get(bridge_to_test).unwrap().unwrap()).unwrap();
}

#[tokio::test]
async fn test_simulate_extra_infos() {
    let extra_info_str = r#"@type bridge-extra-info 1.3
extra-info ElephantBridgeDE2 72E12B89136B45BBC81D1EF0AC7DDDBB91B148DB
master-key-ed25519 eWxjRwAWW7n8BGG9fNa6rApmBFbe3f0xcD7dqwOICW8
published 2024-04-06 03:51:04
transport obfs4
write-history 2024-04-05 04:55:22 (86400 s) 31665735680,14918491136,15423603712,36168353792,40396827648
read-history 2024-04-05 04:55:22 (86400 s) 31799622656,15229917184,15479115776,36317251584,40444155904
ipv6-write-history 2024-04-05 04:55:22 (86400 s) 5972127744,610078720,516897792,327949312,640708608
ipv6-read-history 2024-04-05 04:55:22 (86400 s) 4156158976,4040448000,2935756800,4263080960,6513532928
dirreq-write-history 2024-04-05 04:55:22 (86400 s) 625217536,646188032,618014720,584386560,600778752
dirreq-read-history 2024-04-05 04:55:22 (86400 s) 18816000,19000320,18484224,17364992,18443264
geoip-db-digest 44073997E1ED63E183B79DE2A1757E9997A834E3
geoip6-db-digest C0BF46880C6C132D746683279CC90DD4B2D31786
dirreq-stats-end 2024-04-05 06:51:23 (86400 s)
dirreq-v3-ips ru=16,au=8,by=8,cn=8,gb=8,ir=8,mt=8,nl=8,pl=8,tn=8,tr=8,us=8
dirreq-v3-reqs ru=72,gb=64,pl=32,cn=16,ir=16,us=16,au=8,by=8,mt=8,nl=8,tn=8,tr=8
dirreq-v3-resp ok=216,not-enough-sigs=0,unavailable=0,not-found=0,not-modified=328,busy=0
dirreq-v3-direct-dl complete=0,timeout=0,running=0
dirreq-v3-tunneled-dl complete=212,timeout=4,running=0,min=21595,d1=293347,d2=1624137,q1=1911800,d3=2066929,d4=2415000,md=2888500,d6=3264000,d7=3851333,q3=41>
hidserv-stats-end 2024-04-05 06:51:23 (86400 s)
hidserv-rend-relayed-cells 7924 delta_f=2048 epsilon=0.30 bin_size=1024
hidserv-dir-onions-seen -12 delta_f=8 epsilon=0.30 bin_size=8
hidserv-v3-stats-end 2024-04-05 12:00:00 (86400 s)
hidserv-rend-v3-relayed-cells -4785 delta_f=2048 epsilon=0.30 bin_size=1024
hidserv-dir-v3-onions-seen 5 delta_f=8 epsilon=0.30 bin_size=8
padding-counts 2024-04-05 06:51:42 (86400 s) bin-size=10000 write-drop=0 write-pad=80000 write-total=79980000 read-drop=0 read-pad=1110000 read-total=7989000>
bridge-stats-end 2024-04-05 06:51:44 (86400 s)
bridge-ips ru=40,us=32,??=8,au=8,br=8,by=8,cn=8,de=8,eg=8,eu=8,gb=8,ge=8,hr=8,ie=8,ir=8,kp=8,lt=8,mt=8,nl=8,pl=8,ro=8,sg=8,tn=8,tr=8,vn=8
bridge-ip-versions v4=104,v6=8
bridge-ip-transports <OR>=56,obfs4=56
router-digest-sha256 zK0VMl3i0B2eaeQTR03e2hZ0i8ytkuhK/psgD2J1/lQ
router-digest F30B38390C375E1EE74BFED844177804442569E0"#;

    let extra_info_set = ExtraInfo::parse_file(&extra_info_str);
    assert_eq!(extra_info_set.len(), 1);

    let extra_info = extra_info_set.iter().next().unwrap().clone();

    let extra_info_str = extra_info.to_string();

    let extra_info_2 = ExtraInfo::parse_file(&extra_info_str)
        .into_iter()
        .next()
        .unwrap()
        .clone();
    assert_eq!(extra_info, extra_info_2);

    let bridge_to_test: [u8; 20] =
        array_bytes::hex2array("72E12B89136B45BBC81D1EF0AC7DDDBB91B148DB").unwrap();

    // Open test database
    let db: Db = sled::open("test_db_sei").unwrap();

    // Delete all data in test DB
    db.clear().unwrap();
    assert!(!db.contains_key("bridges").unwrap());
    assert!(!db.contains_key(bridge_to_test).unwrap());

    // Start web server
    spawn(async move {
        extra_infos_server::server().await;
    });

    // Give server time to start
    sleep(Duration::new(1, 0)).await;

    // Update extra-infos (no new data to add)
    update_extra_infos(&db, "http://localhost:8004/")
        .await
        .unwrap();

    // Check that database is still empty
    assert!(!db.contains_key("bridges").unwrap());
    assert!(!db.contains_key(bridge_to_test).unwrap());

    // Add our extra-info to the server's records
    {
        use hyper::{Body, Client, Method, Request};
        let client = Client::new();
        let req = Request::builder()
            .method(Method::POST)
            .uri("http://localhost:8004/add".parse::<hyper::Uri>().unwrap())
            .body(Body::from(serde_json::to_string(&extra_info_set).unwrap()))
            .unwrap();
        client.request(req).await.unwrap();
    }

    // Update extra-infos (add new record)
    update_extra_infos(&db, "http://localhost:8004/")
        .await
        .unwrap();

    // Check that DB now contains information on this bridge
    assert!(db.contains_key("bridges").unwrap());
    let bridges: HashSet<[u8; 20]> =
        bincode::deserialize(&db.get("bridges").unwrap().unwrap()).unwrap();
    assert!(bridges.contains(&bridge_to_test));
    assert!(db.contains_key(bridge_to_test).unwrap());
    let _bridge_info: BridgeInfo =
        bincode::deserialize(&db.get(bridge_to_test).unwrap().unwrap()).unwrap();

    // Add another day's data, with no Russia data this time
    let extra_info_str_2 = r#"@type bridge-extra-info 1.3
extra-info ElephantBridgeDE2 72E12B89136B45BBC81D1EF0AC7DDDBB91B148DB
bridge-stats-end 2024-04-06 06:51:44 (86400 s)
bridge-ips us=32,??=8,au=8,br=8,by=8,cn=8,de=8,eg=8,eu=8,gb=8,ge=8,hr=8,ie=8,ir=8,kp=8,lt=8,mt=8,nl=8,pl=8,ro=8,sg=8,tn=8,tr=8,vn=8"#;

    let extra_info_set_2 = ExtraInfo::parse_file(&extra_info_str_2);

    // Add our extra-info to the server's records
    {
        use hyper::{Body, Client, Method, Request};
        let client = Client::new();
        let req = Request::builder()
            .method(Method::POST)
            .uri("http://localhost:8004/add".parse::<hyper::Uri>().unwrap())
            .body(Body::from(
                serde_json::to_string(&extra_info_set_2).unwrap(),
            ))
            .unwrap();
        client.request(req).await.unwrap();
    }

    // Update extra-infos (add new record)
    update_extra_infos(&db, "http://localhost:8004/")
        .await
        .unwrap();

    let bridge_info: BridgeInfo =
        bincode::deserialize(&db.get(bridge_to_test).unwrap().unwrap()).unwrap();

    // We should not have any data on Canada
    assert!(!bridge_info.info_by_country.contains_key("ca"));

    // We should have data on Russia
    let russia_info = bridge_info.info_by_country.get("ru").unwrap();
    assert_eq!(
        *russia_info
            .info_by_day
            .get(&2460406)
            .unwrap()
            .get(&BridgeInfoType::BridgeIps)
            .unwrap(),
        40
    );
    // Because we have historical data on Russia, but it wasn't listed
    // today, we should see 0
    assert_eq!(
        *russia_info
            .info_by_day
            .get(&2460407)
            .unwrap()
            .get(&BridgeInfoType::BridgeIps)
            .unwrap(),
        0
    );
}
